import sim
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import sys
import os
 
def equi_triangle(y) :
    return y / math.sqrt(3)


def write_data(name, data, path) :
    '''
    params 
    - data : world.data
    '''
    w_cnt = 0
    size = len(data)
    for line in data :
        filename = name + str(line.cnt) + ".txt"
        content = line.show()
        writer = open(path + filename, 'w')
        writer.write(content)
        writer.close()
        print("Logging {:3f}%% completed...".format((w_cnt+1)/size * 100), end='\r')
        w_cnt += 1


world = sim.World()
world.define_height(10)
world.define_tube(equi_triangle, 1)

world.gen_particles()

iteration = int(sys.argv[1])

for i in range(iteration) :
    print("Simulation {:3f}%% completed...".format((i+1)/iteration * 100), end='\r')
    world.update_world()
sys.stdout.flush()
print("Simulation completed...")
#world.show_particles()
#for particle in world.flow :
#    plt.plot(particle.pos[0], particle.pos[1], 'o')





#plt.show()

fig = plt.figure()
plt.xlim(left = -4, right = 4)
plt.ylim(bottom = -11, top = 25)
last = world.data[-1]


#file_name = ""
#content = ""
#for flow in world.data :
#   file_name = "flow" + str(flow.cnt) + ".txt"
#    content = flow.show()
#    f = open(path + file_name, 'w')
#    f.write(content)
#    f.close()
path = os.getcwd() + "/sim_data/"
write_data("flow", world.data, path)

for particle in last.particles :
    plt.plot(particle.pos[0], particle.pos[1], 'o', figure=fig)

# funnel
rs = []
ys = []
for y in range(10) :
    ys.append(y)
    rs.append(equi_triangle(y))
plt.plot(rs, ys, 'r', figure=fig)
for i, r in enumerate(rs) :
    rs[i] = -r
plt.plot(rs, ys, 'r', figure=fig)

plt.show(fig)

