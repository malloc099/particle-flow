import csv
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
import sys
from copy import deepcopy

def equi_triangle(y) :
    return y / np.sqrt(3)

target = int(sys.argv[1])


path = os.getcwd()
path += "/sim_data/"

flow_data = {}

def read_all() :
    size = len(glob.glob(path + "*.txt"))
    for i in range(size) :
        read_a_flow(i + 1)

def read_a_flow(update) :
    file_name = path + "flow" + str(update) + ".txt"
    with open(file_name, 'r') as reader :
        data = csv.reader(reader)
        flow_data[update] = []
        _line = []
        for line in data :
            for i, item in enumerate(line) :
                item = item.strip()
                if item.startswith('(') :
                    left = item.rstrip()
                    right = line[i+1].lstrip()
                    _line.append(left + ", " + right)
                elif item.endswith(')') :
                    pass
                else :
                    _line.append(item)
            flow_data[update].append(_line)
            _line = []

#read_a_flow(target)
#for line in flow_data[target] :
#    print(line)

def show_all() :
    for update, item in flow_data.items() :
        print(update)
        for line in item :
            print(line)


def plot_particles(update) :
    pos = []
    for data in flow_data[update] :
        p= data[0].strip('()').split(',')
        _pos = float(p[0]), float(p[1])
        pos.append(_pos)
    for p in pos :
        plt.plot(p[0], p[1], 'o', figure=fig)
    plt.show()

fig = plt.figure()

plt.xlim(left = -4, right = 4)
plt.ylim(bottom = -11, top = 25)

rs = []
ys = []
for y in range(10) :
    ys.append(y)
    rs.append(equi_triangle(y))
plt.plot(rs, ys, 'r', figure=fig)
for i, r in enumerate(rs) :
    rs[i] = -r
plt.plot(rs, ys, 'r', figure=fig)
plt.plot([-10, 10], [-10, -10], figure=fig)

read_all()
#show_all()
plot_particles(target)


